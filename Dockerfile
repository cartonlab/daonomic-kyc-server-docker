FROM debian:9

RUN apt-get update && apt-get install -y openjdk-8-jdk openjdk-8-jre-headless git maven

RUN echo "VERSION 6422030"

RUN cd /usr/src && git clone https://github.com/daonomic/daonomic-kyc-server && cd daonomic-kyc-server && mvn clean package -DskipTests
RUN apt-get install -y cron logrotate nginx certbot

RUN apt-get purge -y openjdk-8-jdk git maven

COPY ./nginx/dhparams.pem /etc/nginx/dhparams.pem
COPY ./nginx/nginx.conf /etc/nginx/nginx.conf
COPY ./nginx/options-ssl-nginx.conf /etc/nginx/options-ssl-nginx.conf
COPY ./nginx/conf.d/* /etc/nginx/conf.d/

RUN rm -rf /var/lib/apt/lists/*

RUN mkdir /tmp/.well-known && echo "alive" > /tmp/.well-known/alive.txt

RUN mkdir /var/lib/kyc && mkdir /var/log/kyc
RUN echo "#!/bin/bash" > /usr/local/bin/kyc-entrypoint.sh

RUN echo "service cron restart; service nginx restart;" >> /usr/local/bin/kyc-entrypoint.sh
RUN echo "cd /usr/src/daonomic-kyc-server && java -DhttpPort=8081 -DdataPath=/var/lib/kyc -jar target/kyc-server-boot.jar > /var/log/kyc/kyc.log 2> /var/log/kyc/errors.log;" >> /usr/local/bin/kyc-entrypoint.sh

RUN chmod 755 /usr/local/bin/kyc-entrypoint.sh
RUN ln -s usr/local/bin/kyc-entrypoint.sh /

ENTRYPOINT ["kyc-entrypoint.sh"]
