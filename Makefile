SHELL := /bin/bash
TMP=$(shell realpath "../../tmp")

IMAGE=daonomic-kyc-server
DATA=/opt/volumes/${IMAGE}/data
LOG_KYC=/opt/volumes/${IMAGE}/log/kyc
LOG_NGINX=/opt/volumes/${IMAGE}/log/nginx
CONF_NGINX=/opt/volumes/${IMAGE}/conf/nginx
NETWORK=kyc_network

.DEFAULT: default


default: all

all: stop build run

build:
	docker build -t ${IMAGE} . -f Dockerfile

run:
	docker stop ${IMAGE}; docker rm ${IMAGE}; \
	docker run --name ${IMAGE} -d \
		-p 80:80 \
		-p 443:443 \
		-v ${DATA}:/var/lib/kyc \
		-v ${LOG_KYC}:/var/log/kyc \
		-v ${LOG_NGINX}:/var/log/nginx \
		-dit --restart unless-stopped ${IMAGE}


stop:
	-docker stop ${IMAGE}
	docker ps -a


network-create:
	docker network rm ${NETWORK}; \
	docker network create --driver bridge ${NETWORK};
	docker network ls;
	docker network inspect ${NETWORK};

network-delete:
	docker network rm ${NETWORK}; \
	docker network ls;
