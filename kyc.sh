#!/bin/bash

DOCKERREPO='https://gitlab.com/cartonlab/daonomic-kyc-server-docker'
DOMAIN=''
EMAIL=''

echo '======'
echo "Будет выполнена установка KYC-сервера"
echo "Для успешной установки порт 80 и 443 НЕ должны быть заняты!"
echo "И еще потребуются права sudo"
echo "Для продолжения жми любую кнопку, для остановки - Ctrl+C"
read -n 1

sudo apt-get update
sudo apt-get install -y net-tools

LISTEN=`sudo netstat -natp | grep -P ":(443|80)\s.*LISTEN"`

if [[ $LISTEN ]] ; then
   echo '======'
   echo "Порты заняты. Продолжить не могу."
   sudo netstat -natp | grep -P ":(443|80)\s.*LISTEN"
   exit
fi

echo '======> Удаляем docker'
sudo apt-get remove -y docker docker-engine docker.io
sudo apt-get install -y git curl wget build-essential apt-transport-https ca-certificates software-properties-common


echo '======> Устанавливаем docker-ce'
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88

sudo rm /etc/apt/sources.list.d/docker-ce.list;
echo "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | sudo tee -a /etc/apt/sources.list.d/docker-ce.list

sudo apt-get update

sudo apt-get install -y docker-ce

sudo groupadd docker

echo '======> Создаем и запускаем docker-container'
git clone "${DOCKERREPO}" kyc;
cd kyc;
sudo make -f Makefile

echo '======> Делаем SSL сертификаты'
echo "Сейчас будем выпускать SSL сертификаты"
echo "Для этого нужен emeil и имя домена"

while [ "$EMAIL" = "" ]; do
    echo -n "E-Mail: "
    read EMAIL
done


while [ "$DOMAIN" = "" ]; do
    echo -n "Имя домена: "
    read DOMAIN
done

echo "! Кроме того спросят про webroot. Нужно будет ввести: /tmp"
echo "Для продолжения жми любую кнопку, для остановки - Ctrl+C"
read -n 1

sudo docker exec -it daonomic-kyc-server certbot certonly --agree-tos -m ${EMAIL} --webroot -d ${DOMAIN}
sudo docker exec -it daonomic-kyc-server ln -s /etc/letsencrypt/live/${DOMAIN}/fullchain.pem /etc/letsencrypt/live/default_cert.pem
sudo docker exec -it daonomic-kyc-server ln -s /etc/letsencrypt/live/${DOMAIN}/privkey.pem /etc/letsencrypt/live/default_key.pem
sudo docker exec -it daonomic-kyc-server sed -ri 's/#SSLCONF\s*include/include/' /etc/nginx/conf.d/kyc.conf;
sudo docker exec -it daonomic-kyc-server service nginx restart;

echo '======='
echo "https://${DOMAIN}"
